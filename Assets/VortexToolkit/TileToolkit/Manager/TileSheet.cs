using System;
using UnityEngine;

public class TileSheet {
	private string _name;
	private Material _material;
	private int _firstId = 0;

	private int _margin = 0;
	private int _spacing = 0;

	private Vector2 _textureSize = new Vector2( 0,0 );
	private Vector2 _tileSize = new Vector2( 0,0 );
	private Vector2 _tileCount = new Vector2( 0,0 );

	public string Name {
		get { return _name; }
	}

	public Material Material {
		get { return _material; }
	}

	public int FirstGID {
		get { return _firstId; }
	}

	public TileSheet ( JSONObject sheetJson ) {
		// Get the first tile GID
		_firstId = (int)(sheetJson.GetField("firstgid").n);

		// Load the texture
		_name = sheetJson.GetField("name").str;
		_material = Resources.Load<Material>( _name );
		_textureSize = new Vector2( sheetJson.GetField("imagewidth").n,
		                            sheetJson.GetField("imageheight").n );

		// Load the tile data
		_tileSize = new Vector2( sheetJson.GetField("tilewidth").n,
		                         sheetJson.GetField("tileheight").n );
		_margin = (int)(sheetJson.GetField("margin").n);
		_spacing = (int)(sheetJson.GetField("spacing").n);

		// Calculate how many tiles we have
		_tileCount.x = Mathf.Floor(_textureSize.x / (_tileSize.x+_spacing));
		_tileCount.y = Mathf.Floor(_textureSize.y / (_tileSize.y+_spacing));
	}

	public void GetUV( int tileGID, out Vector2 minUV, out Vector2 maxUV ) {
		tileGID -= _firstId;

		// Convert the GID to tile position
		Vector2 tilePos = new Vector2();

		tilePos.x = tileGID % _tileCount.x;
		tilePos.y = Mathf.Floor( tileGID / _tileCount.x );

		// Calculate the UV
		minUV = new Vector2( tilePos.x / (_textureSize.x / (_tileSize.x+_spacing)),
		                    -tilePos.y / (_textureSize.y / (_tileSize.y+_spacing)) );
		maxUV = new Vector2( (tilePos.x+1.0f) / (_textureSize.x / (_tileSize.x+_spacing)),
		                    -(tilePos.y+1.0f) / (_textureSize.y / (_tileSize.y+_spacing)) );

		// Calculate marging and spacing
		maxUV.x -= (1.0f/_textureSize.x) * _spacing;
		maxUV.y += (1.0f/_textureSize.y) * _spacing;

		//Debug.Log( (1.0f/_textureSize.x) * _spacing );

		// Apply bleeding area
		return;
		Vector2 bleeding = new Vector2( (1.0f/_textureSize.x)*1,
		                                (1.0f/_textureSize.y)*1 );
		minUV.x += bleeding.x;
		minUV.y -= bleeding.y;
		
		maxUV.x -= bleeding.y;
		maxUV.y += bleeding.y;
	}
}