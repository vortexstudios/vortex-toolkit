﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]

[ExecuteInEditMode]
public class TileLayer : MonoBehaviour {
	protected TileManager _parent;
	public TileManager Parent {
		get { return _parent; }
		set { 
			_parent = value;
			this.transform.parent = this._parent.transform;
		}
	}

	protected JSONObject _jsonData;
	public JSONObject JSONData {
		get { return _jsonData; }
		set { 
			_jsonData = value;
			int minGID, maxGID;

			for( var q = 0; q < _parent.Sheets.Count; q++ ) {
				minGID = _parent.Sheets[q].FirstGID;
				if( q == _parent.Sheets.Count-1 ) 
					maxGID = 9999999;
				else
					maxGID = _parent.Sheets[q+1].FirstGID;

				this.buildMesh( _parent.Sheets[q], minGID, maxGID );
			}
		}
	}

	// LAYER DATA
	private Vector2 _mapSize = new Vector2(0,0);
	private Vector2 _tileSize = new Vector2(0,0);

	// MESH BUILDER
	private Vector3[] _meshVerts = new Vector3[4];
	private Vector3[] _meshNormals = new Vector3[4];
	private Vector2[] _meshUV = new Vector2[4];
	private int[] _meshTri = new int[6];
	
	// Use this for initialization
	void Start () { }
	
	// Update is called once per frame
	void Update () { }

	void OnDrawGizmos() {
		return;
		Gizmos.color = Color.grey;

		// Draw grid
		for( int q = 0; q <= _mapSize.x; q++ ) {

			Gizmos.DrawLine( new Vector3(this.transform.position.x+q*_tileSize.x, this.transform.position.y, this.transform.position.z), 
			                new Vector3(this.transform.position.x+q*_tileSize.x, this.transform.position.y-_mapSize.y*_tileSize.y, this.transform.position.z) );		

			for( int w = 0; w <= _mapSize.y; w++ ) {
				Gizmos.DrawLine( new Vector3(this.transform.position.x, this.transform.position.y-w*_tileSize.y, this.transform.position.z), 
				                new Vector3(this.transform.position.x+_mapSize.x*_tileSize.x, this.transform.position.y-w*_tileSize.y, this.transform.position.z) );		
			}
		}
	}
	
	private void buildMesh( TileSheet sheet, int minGID, int maxGID ) {
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		List<int> triangles = new List<int>();
		int triangleFaces = -1;

		_mapSize = new Vector2( _jsonData.GetField("width").f, 
		                        _jsonData.GetField("height").f );

		_tileSize = new Vector2( 70.0f, 70.0f );

		// Draw tiles
		for( int x = 0; x < _mapSize.x; x++ ) {
			for( int y = 0; y < _mapSize.y; y++ ) {
				float tileId = _jsonData.GetField("data").list[x + (y * (int)(_mapSize.x))].f;

				// Just draw the tiles for this tilesheet, and never draw the clear tile (GID 0), it's a waste of memory xD
				if( tileId >= minGID && tileId < maxGID ) {
					float width = _tileSize.x;
					float height = _tileSize.y;

					// Vertices
					vertices.Add( new Vector3((_tileSize.x*x), -(_tileSize.y*y+height), 0) );
					vertices.Add( new Vector3( width+(_tileSize.x*x), -(_tileSize.y*y+height), 0) );
					vertices.Add( new Vector3((_tileSize.x*x),  -(_tileSize.y*y), 0) );
					vertices.Add( new Vector3( width+(_tileSize.x*x),	-(_tileSize.y*y), 0) );

					// Triangles
					triangleFaces += 4;

					triangles.Add(triangleFaces - 3);
					triangles.Add(triangleFaces - 1);
					triangles.Add(triangleFaces - 2);		
					triangles.Add(triangleFaces - 2);
					triangles.Add(triangleFaces - 1);
					triangles.Add(triangleFaces);

					// UV
					Vector2 minUV = new Vector2();
					Vector2 maxUV = new Vector2();

					sheet.GetUV( (int)(tileId), out minUV, out maxUV );
					uvs.Add( new Vector2( minUV.x, maxUV.y ) );
					uvs.Add( new Vector2( maxUV.x, maxUV.y ) );
					uvs.Add( new Vector2( minUV.x, minUV.y ) );
					uvs.Add( new Vector2( maxUV.x, minUV.y ) );

				}
			}
		}

		GameObject tempGameObject = new GameObject( this.transform.parent.name + "_" + sheet.Name );
		tempGameObject.AddComponent<MeshFilter>();
		tempGameObject.AddComponent<MeshRenderer>();

		tempGameObject.transform.parent = this.transform;
		tempGameObject.renderer.transform.position = this.transform.position;//new Vector3( 0.0f, 0.0f, 0.0f );
		tempGameObject.renderer.transform.Rotate(0,0,0);

		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.uv = uvs.ToArray();

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		mesh.Optimize();

		MeshFilter mf = (MeshFilter)tempGameObject.GetComponent(typeof(MeshFilter));
		MeshRenderer mr = (MeshRenderer)tempGameObject.GetComponent(typeof(MeshRenderer));
		mesh.name = this.transform.parent.name + "_" + sheet.Name;
		mf.mesh = mesh;
		mr.renderer.material = sheet.Material;//.color = Color.white;
		//mf.renderer.material.mainTexture = sheet.Texture;
	}
}
