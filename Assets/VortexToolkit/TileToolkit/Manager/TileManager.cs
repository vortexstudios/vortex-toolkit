﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[ExecuteInEditMode]
public class TileManager : MonoBehaviour {
	protected List<TileLayer> Layer = new List<TileLayer>();
	protected List<TileSheet> Sheet = new List<TileSheet>();

	public List<TileLayer> Layers {
		get { return Layer; }
	}

	public List<TileSheet> Sheets {
		get { return Sheet; }
	}

	// Use this for initialization
	void Awake () {
		// Clear current childs
		List<GameObject> children = new List<GameObject>();
		foreach( Transform child in transform ) children.Add( child.gameObject );
		children.ForEach( child => DestroyImmediate( child, true ) );
		foreach( Transform child in transform ) { DestroyImmediate( child, true ); }

		// Load the file
		TextAsset asset = Resources.Load<TextAsset>("level2");
		JSONObject tileJson = new JSONObject( asset.text );

		// Load texture sheet
		foreach( JSONObject sheetJson in tileJson.GetField("tilesets").list ) {
			AddSheet( sheetJson );
		}

		// Create layers
		foreach( JSONObject layerJson in tileJson.GetField("layers").list ) {
			switch( layerJson.GetField("type").str ) {
				case "tilelayer":
					AddLayer( layerJson );
					break;

				case "objectgroup":
					AddMesh( layerJson );
					break;

				default:
					Debug.LogWarning( "TileManager : [" + layerJson.GetField("type").str.ToUpper() + "] Type Not Supported" );
					break;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void AddSheet( JSONObject sheetJson ) {
		TileSheet tempTileSheet = new TileSheet( sheetJson );

		Sheet.Add( tempTileSheet );
	}

	public void AddMesh( JSONObject layerJson ) {
		GameObject tempGameObject = new GameObject( layerJson.GetField("name").str );
		tempGameObject.AddComponent<MeshFilter>();
		tempGameObject.AddComponent<MeshRenderer>();

		tempGameObject.transform.parent = this.transform;
		tempGameObject.renderer.transform.position = new Vector3( 0.0f, 0.0f, 100.0f );
		tempGameObject.renderer.transform.Rotate(0,0,0);	

		foreach( JSONObject meshJson in layerJson.GetField("objects").list ) {
			Triangulator t = new Triangulator();
			List<Vector2> Pontos = new List<Vector2>();

			foreach( JSONObject pointJson in meshJson.GetField("polyline").list ) {
				Pontos.Add( new Vector2( pointJson.GetField("x").n, -pointJson.GetField("y").n ) );
			}

			GameObject meshGameObject = new GameObject();//t.CreateInfluencePolygon( Pontos.ToArray() );
			meshGameObject.AddComponent<MeshFilter>();
			meshGameObject.AddComponent<MeshRenderer>();
			meshGameObject.AddComponent<PolygonCollider2D>();
			meshGameObject.GetComponent<PolygonCollider2D>().points = Pontos.ToArray();
			meshGameObject.isStatic = true;

			meshGameObject.transform.parent = tempGameObject.transform;
			meshGameObject.renderer.transform.position = new Vector3( meshJson.GetField("x").n, -meshJson.GetField("y").n, -200.0f );
			meshGameObject.renderer.transform.Rotate(0,0,0);
		}
	}

	public void AddLayer( JSONObject layerJson ) {
		GameObject tempGameObject = new GameObject( layerJson.GetField("name").str );
		tempGameObject.AddComponent<TileLayer>();

		tempGameObject.transform.parent = this.transform;
		tempGameObject.renderer.transform.position = new Vector3( 0.0f, 0.0f, -50*Layer.Count );
		tempGameObject.renderer.transform.Rotate(0,0,0);	

		tempGameObject.GetComponent<TileLayer>().Parent = this;
		tempGameObject.GetComponent<TileLayer>().JSONData = layerJson;
		Layer.Add( tempGameObject.GetComponent<TileLayer>() );
	}
}
